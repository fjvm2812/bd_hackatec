create database hackatec;
use hackatec;
CREATE TABLE IF NOT EXISTS Estados (
  idEstados INT NOT NULL,
  NombreEstado VARCHAR(45) NOT NULL,
  PRIMARY KEY (idEstados))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS Municipios (
  Nombre_Municipio VARCHAR(45) NOT NULL,
  Estados_idEstados INT NOT NULL,
  PRIMARY KEY (Nombre_Municipio, Estados_idEstados),
  INDEX  (Estados_idEstados),
  CONSTRAINT FOREIGN KEY (Estados_idEstados)REFERENCES Estados (idEstados))
ENGINE = InnoDB;
 
CREATE TABLE IF NOT EXISTS Tamaño_Localidad (
  idTamaño_Localidad INT NOT NULL,
  Tamaño_Localidadcol VARCHAR(45) NOT NULL,
  PRIMARY KEY (idTamaño_Localidad))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS Localidad (
  Nombre_Localidad VARCHAR(45) NOT NULL,
  IdEstados INT NOT NULL,
  Municipio VARCHAR(45) NOT NULL,
  Tamaño_Localidad_idTamaño_Localidad INT NOT NULL,
  PRIMARY KEY (Nombre_Localidad, IdEstados, Municipio, Tamaño_Localidad_idTamaño_Localidad),
  INDEX  (Municipio, IdEstados),
  INDEX  (Tamaño_Localidad_idTamaño_Localidad ),
  CONSTRAINT FOREIGN KEY (Municipio , IdEstados)REFERENCES Municipios (Nombre_Municipio , Estados_idEstados),
  CONSTRAINT FOREIGN KEY (Tamaño_Localidad_idTamaño_Localidad)REFERENCES Tamaño_Localidad (idTamaño_Localidad))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS Estado_Conyugal (
  idEstado_Conyugal INT NOT NULL,
  Nombre_estado_conyugal VARCHAR(45) NOT NULL,
  PRIMARY KEY (idEstado_Conyugal))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS Escolaridad (
  idEscolaridad INT NOT NULL,
  Nombre_Escolaridad VARCHAR(45) NOT NULL,
  PRIMARY KEY (idEscolaridad))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS Derechohabiencia (
  idDerechohabiencia INT NOT NULL,
  Nombre_Derechohabiencia VARCHAR(45) NOT NULL,
  PRIMARY KEY (idDerechohabiencia))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS Sitio_Defuncion (
  idSitio_Defuncion INT NOT NULL,
  Nombre_Sitio_Defuncion VARCHAR(45) NOT NULL,
  PRIMARY KEY (idSitio_Defuncion))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS Mes (
  idMes INT NOT NULL,
  Nombre_Mes VARCHAR(45) NOT NULL,
  PRIMARY KEY (idMes))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS Asistencia(
  idAsistencia INT NOT NULL,
  Nombre_Asistencia VARCHAR(45) NOT NULL,
  PRIMARY KEY (idAsistencia))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS CAUSA_CIE_4 (
  idCAUSA_CIE_4 varchar(50) NOT NULL,
  Nombre_CAUSA_CIE_4 VARCHAR(45) NOT NULL,
  PRIMARY KEY (idCAUSA_CIE_4))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS CERTIFICO (
  idCERTIFICO INT NOT NULL,
  Nombre_CERTIFICO VARCHAR(45) NULL,
  PRIMARY KEY (idCERTIFICO))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS RAZON_MORTALIDAD_MATERNA(
  idRAZON_MORTALIDAD_MATERNA INT NOT NULL,
  Nom_RAZON_MORTALIDAD_MATERNAl VARCHAR(75) NOT NULL,
  PRIMARY KEY (idRAZON_MORTALIDAD_MATERNA))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS EDAD_QUINQUENAL (
  idEDAD_QUINQUENAL INT NOT NULL,
  des_EDAD_QUINQUENAL VARCHAR(45) NULL,
  PRIMARY KEY (idEDAD_QUINQUENAL))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS Ocupacion (
  idOcupacion INT NOT NULL,
  Nombre_Ocupacion VARCHAR(45) NOT NULL,
  PRIMARY KEY (idOcupacion))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS Caso (
  id_caso INT NOT NULL,
  Año_Nacimiento INT NOT NULL,
  Mes_idMes INT NOT NULL,
  Dia_Nacimiento INT NOT NULL,
  Edad INT NOT NULL,
  Estado_Conyugal_idEstado_Conyugal INT NOT NULL,
  Localidad_Municipios_Estados_idEstados INT NOT NULL,
  Localidad_Municipios_idMunicipio varchar(50) NOT NULL,
  Localidad_Nombre_Localidad VARCHAR(45) NOT NULL,
  Ocupacion_idOcupacion INT NOT NULL,
  Escolaridad_idEscolaridad INT NOT NULL,
  Derechohabiencia_idDerechohabiencia INT NOT NULL,
  Localidad_Municipios_Estados_idEstados1 INT NOT NULL,
  Localidad_Municipios_idMunicipio1 varchar(45) NOT NULL,
  Localidad_Nombre_Localidad1 VARCHAR(45) NOT NULL,
  Sitio_Defuncion_idSitio_Defuncion INT NOT NULL,
  Año_Defuncion INT NOT NULL,
  Mes_Defuncion INT NOT NULL,
  Dia INT NOT NULL,
  Hora INT NOT NULL,
  Minutos INT NOT NULL,
  Asistencia_idAsistencia INT NOT NULL,
  CAUSA_CIE_4_idCAUSA_CIE_4 varchar(45) NOT NULL,
  CERTIFICO_idCERTIFICO INT NOT NULL,
  Localidad_Municipios_Estados_idEstados2 INT NOT NULL,
  Localidad_Municipios_idMunicipio2 varchar(45) NOT NULL,
  Año_Registro INT NOT NULL,
  Mes_idMes2 INT NOT NULL,
  Dia_registro INT NOT NULL,
  Año_Certificado INT NOT NULL,
  Mes_idMes3 INT NOT NULL,
  Dia_Certificado INT NOT NULL,
  Año_bd INT NOT NULL,
  RAZON_MORTALIDAD_MATERNA_idRAZON_MORTALIDAD_MATERNA INT NOT NULL,
  EDAD_QUINQUENAL_idEDAD_QUINQUENAL INT NOT NULL,
  PRIMARY KEY (id_caso, Mes_idMes, Estado_Conyugal_idEstado_Conyugal, Localidad_Municipios_Estados_idEstados, Localidad_Municipios_idMunicipio, Localidad_Nombre_Localidad, Ocupacion_idOcupacion, Escolaridad_idEscolaridad, Derechohabiencia_idDerechohabiencia, Localidad_Municipios_Estados_idEstados1, Localidad_Municipios_idMunicipio1, Localidad_Nombre_Localidad1, Sitio_Defuncion_idSitio_Defuncion, Mes_Defuncion, Asistencia_idAsistencia, CAUSA_CIE_4_idCAUSA_CIE_4, CERTIFICO_idCERTIFICO, Localidad_Municipios_Estados_idEstados2, Localidad_Municipios_idMunicipio2, Mes_idMes2, Mes_idMes3, RAZON_MORTALIDAD_MATERNA_idRAZON_MORTALIDAD_MATERNA, EDAD_QUINQUENAL_idEDAD_QUINQUENAL),
  INDEX (Mes_idMes),
  INDEX  (Estado_Conyugal_idEstado_Conyugal) ,
  INDEX  (Localidad_Nombre_Localidad , Localidad_Municipios_Estados_idEstados, Localidad_Municipios_idMunicipio ),
  INDEX  (Ocupacion_idOcupacion ),
  INDEX  (Escolaridad_idEscolaridad ) ,
  INDEX  (Derechohabiencia_idDerechohabiencia ),
  INDEX  (Localidad_Nombre_Localidad1 , Localidad_Municipios_Estados_idEstados1 , Localidad_Municipios_idMunicipio1 ),
  INDEX  (Sitio_Defuncion_idSitio_Defuncion ),
  INDEX  (Mes_Defuncion ),
  INDEX  (Asistencia_idAsistencia ) ,
  INDEX  (CAUSA_CIE_4_idCAUSA_CIE_4 ) ,
  INDEX  (CERTIFICO_idCERTIFICO ) ,
  INDEX  (Localidad_Municipios_Estados_idEstados2 , Localidad_Municipios_idMunicipio2 ) ,
  INDEX  (Mes_idMes2 ) ,
  INDEX (Mes_idMes3 ) ,
  INDEX  (RAZON_MORTALIDAD_MATERNA_idRAZON_MORTALIDAD_MATERNA ) ,
  INDEX  (EDAD_QUINQUENAL_idEDAD_QUINQUENAL ))
ENGINE = InnoDB;
ALTER TABLE Caso ADD CONSTRAINT 
FOREIGN KEY (Mes_idMes) REFERENCES Mes(idMes)
ON UPDATE CASCADE
ON DELETE RESTRICT;
ALTER TABLE Caso ADD CONSTRAINT 
FOREIGN KEY (Estado_Conyugal_idEstado_Conyugal) REFERENCES Estado_Conyugal(idEstado_Conyugal)
ON UPDATE CASCADE
ON DELETE RESTRICT;
-- ALTER TABLE Caso ADD CONSTRAINT 
-- FOREIGN KEY (Localidad_Nombre_Localidad, Localidad_Municipios_Estados_idEstados, Localidad_Municipios_idMunicipio, Localidad_Tamaño_Localidad_idTamaño_Localidad) 
-- REFERENCES Localidad (Nombre_Localidad, Municipios_Estados_idEstados, Municipios_idMunicipio,Tamaño_Localidad_idTamaño_Localidad);
ALTER TABLE Caso ADD CONSTRAINT 
FOREIGN KEY (Ocupacion_idOcupacion) REFERENCES Ocupacion(idOcupacion)
ON UPDATE CASCADE
ON DELETE RESTRICT;
 ALTER TABLE Caso ADD CONSTRAINT 
FOREIGN KEY (Escolaridad_idEscolaridad) REFERENCES Escolaridad(idEscolaridad)
ON UPDATE CASCADE
ON DELETE RESTRICT;
ALTER TABLE Caso ADD CONSTRAINT 
FOREIGN KEY (Derechohabiencia_idDerechohabiencia) REFERENCES Derechohabiencia(idDerechohabiencia)
ON UPDATE CASCADE
ON DELETE RESTRICT;
--  ALTER TABLE Caso ADD CONSTRAINT 
-- FOREIGN KEY (Localidad_Nombre_Localidad1 , Localidad_Municipios_Estados_idEstados1 , Localidad_Municipios_idMunicipio1) 
-- REFERENCES Localidad (Nombre_Localidad , Municipios_Estados_idEstados , Municipios_idMunicipio)
-- ON UPDATE CASCADE
-- ON DELETE RESTRICT;
  ALTER TABLE Caso ADD CONSTRAINT 
FOREIGN KEY (Sitio_Defuncion_idSitio_Defuncion) REFERENCES Sitio_Defuncion(idSitio_Defuncion)
ON UPDATE CASCADE
ON DELETE RESTRICT;
  ALTER TABLE Caso ADD CONSTRAINT 
FOREIGN KEY (Mes_Defuncion) REFERENCES Mes(idMes)
ON UPDATE CASCADE
ON DELETE RESTRICT;
  ALTER TABLE Caso ADD CONSTRAINT 
FOREIGN KEY (Asistencia_idAsistencia) REFERENCES Asistencia(idAsistencia)
ON UPDATE CASCADE
ON DELETE RESTRICT;
    ALTER TABLE Caso ADD CONSTRAINT 
FOREIGN KEY (CAUSA_CIE_4_idCAUSA_CIE_4) REFERENCES CAUSA_CIE_4(idCAUSA_CIE_4)
ON UPDATE CASCADE
ON DELETE RESTRICT;
   ALTER TABLE Caso ADD CONSTRAINT 
FOREIGN KEY (CERTIFICO_idCERTIFICO) REFERENCES CERTIFICO(idCERTIFICO)
ON UPDATE CASCADE
ON DELETE RESTRICT;
  --   ALTER TABLE Caso ADD CONSTRAINT 
-- FOREIGN KEY (Localidad_Municipios_Estados_idEstados2 , Localidad_Municipios_idMunicipio2) REFERENCES Localidad (Municipios_Estados_idEstados , Municipios_idMunicipio)
-- ON UPDATE CASCADE
-- ON DELETE RESTRICT;
    ALTER TABLE Caso ADD CONSTRAINT 
FOREIGN KEY (Mes_idMes2) REFERENCES Mes(idMes)
ON UPDATE CASCADE
ON DELETE RESTRICT;
   ALTER TABLE Caso ADD CONSTRAINT 
FOREIGN KEY (Mes_idMes3) REFERENCES Mes(idMes)
ON UPDATE CASCADE
ON DELETE RESTRICT;
    ALTER TABLE Caso ADD CONSTRAINT 
FOREIGN KEY (RAZON_MORTALIDAD_MATERNA_idRAZON_MORTALIDAD_MATERNA) REFERENCES RAZON_MORTALIDAD_MATERNA(idRAZON_MORTALIDAD_MATERNA)
ON UPDATE CASCADE
ON DELETE RESTRICT;
  ALTER TABLE Caso ADD CONSTRAINT 
FOREIGN KEY (EDAD_QUINQUENAL_idEDAD_QUINQUENAL) REFERENCES EDAD_QUINQUENAL(idEDAD_QUINQUENAL)
ON UPDATE CASCADE
ON DELETE RESTRICT;
select * from Caso;